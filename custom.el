(add-hook 'cuda-mode-hook 'general-code-hook)
(add-hook 'python-mode-hook 'general-code-hook)
(add-to-list 'auto-mode-alist '("\\.cl\\'" . c-mode))
(defun general-code-hook ()
  (highlight-indentation-mode 1)
  (whitespace-mode 0)
  (flyspell-mode 0))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 ;;'(desktop-save-mode t)
 '(package-selected-packages
   (quote
    (d-mode pkgbuild-mode markdown-mode zop-to-char zenburn-theme which-key volatile-highlights undo-tree smartrep smartparens smart-mode-line operate-on-number move-text magit projectile ov imenu-anywhere guru-mode grizzl god-mode gitignore-mode gitconfig-mode git-timemachine gist expand-region epl editorconfig easy-kill diminish diff-hl discover-my-major dash crux browse-kill-ring beacon anzu ace-window))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
